package main

import (
	"doctors/utils"
	"fmt"
)

type Appointment struct {
	ID                   string
	DoctorID             string
	SubTreatmentDuration int
	AppointmentedAt      string
}

func appointmentMock() []Appointment {
	return []Appointment{
		{
			ID:                   "1",
			DoctorID:             "D1",
			SubTreatmentDuration: 5,
			AppointmentedAt:      "09:00",
		},
		{
			ID:                   "2",
			DoctorID:             "D1",
			SubTreatmentDuration: 5,
			AppointmentedAt:      "09:00",
		},
		{
			ID:                   "3",
			DoctorID:             "D1",
			SubTreatmentDuration: 45,
			AppointmentedAt:      "09:30",
		},
		{
			ID:                   "4",
			DoctorID:             "D2",
			SubTreatmentDuration: 9,
			AppointmentedAt:      "09:00",
		},
		{
			ID:                   "5",
			DoctorID:             "D2",
			SubTreatmentDuration: 12,
			AppointmentedAt:      "09:00",
		},
		{
			ID:                   "6",
			DoctorID:             "D2",
			SubTreatmentDuration: 65,
			AppointmentedAt:      "09:30",
		},
	}
}

func doctorPeriodMock() map[string]map[string]int {
	doctorPeriods := make(map[string]map[string]int)
	doctorPeriods["D1"] = map[string]int{
		"09:00": 0,
		"09:30": 0,
		"10:00": 0,
		"10:30": 0,
		"11:00": 0,
		"11:30": 0,
	}
	doctorPeriods["D2"] = map[string]int{
		"09:00": 0,
		"09:30": 0,
		"10:00": 0,
		"10:30": 0,
		"11:00": 0,
		"11:30": 0,
	}

	return doctorPeriods
}

func main() {
	doctorPeriods := doctorPeriodMock()
	appointments := appointmentMock()
	resDoctors := []string{"D1", "D2"}
	periodList := []string{"09:00", "09:30", "10:00", "10:30"}

	subTreatmentDuration := 15
	maxDuration := 30

	// init doctor periods
	for _, appointment := range appointments {
		totalDuration := doctorPeriods[appointment.DoctorID][appointment.AppointmentedAt] + appointment.SubTreatmentDuration

		index := 0
		for subPeriod := totalDuration; subPeriod > 0; subPeriod -= maxDuration {
			periodIndex := utils.GetIndexByValue(periodList, appointment.AppointmentedAt)
			nextPeriodIndex := periodIndex + index
			if periodIndex < 0 || nextPeriodIndex > len(periodList)-1 {
				break
			}

			// Prevent doctor out of period
			if _, ok := doctorPeriods[appointment.DoctorID][periodList[nextPeriodIndex]]; ok {
				if totalDuration > maxDuration {
					doctorPeriods[appointment.DoctorID][periodList[nextPeriodIndex]] = maxDuration
				} else {
					doctorPeriods[appointment.DoctorID][periodList[nextPeriodIndex]] = subPeriod
				}
			}

			totalDuration -= maxDuration
			index += 1
		}
	}

	fmt.Printf("\n %+v \n", resDoctors)
	fmt.Printf("\n %+v \n", doctorPeriods)
	// Test periods sub treatment duration
	for doctorKey := range doctorPeriods {
		fmt.Printf("\n doctorKey: %v", doctorKey)

		// Sort doctor's periods
		periods := []string{}
		for periodKey := range doctorPeriods[doctorKey] {
			periods = append(periods, periodKey)
		}

		periods = utils.SortTimes(periods)

		for _, periodKey := range periods {
			fmt.Printf("\n periodKey A: %v", periodKey)
			if doctorPeriods[doctorKey][periodKey] >= maxDuration {
				delete(doctorPeriods[doctorKey], periodKey)
			} else {
				totalDuration := doctorPeriods[doctorKey][periodKey] + subTreatmentDuration
				fmt.Printf("\n %v %v | %v + %v = %v", doctorKey, periodKey, doctorPeriods[doctorKey][periodKey], subTreatmentDuration, totalDuration)

				index := 0
				for subPeriod := totalDuration; subPeriod > 0; subPeriod -= maxDuration {
					// Map cannot get by index thereforce make periodList can be get by index
					periodIndex := getIndexByValue(periods, periodKey)
					nextPeriodIndex := periodIndex + index
					if periodIndex < 0 || nextPeriodIndex > len(periods)-1 {
						// When last period but subPeriod still more than 0
						if subPeriod > 0 {
							fmt.Printf("\n deleted B: %v", periodKey)
							delete(doctorPeriods[doctorKey], periodKey)
						}

						break
					}

					nextPeriod := periodIndex + index
					totalPeriod := 0
					if totalDuration > maxDuration {
						totalPeriod = maxDuration
					} else {
						totalPeriod = subPeriod
					}

					isUseMoreThanOnePeriod := index > 0
					nextPeriodHasTask := doctorPeriods[doctorKey][periods[nextPeriod]] != 0
					nextPeriodMoreThanTotalPeriod := (maxDuration - doctorPeriods[doctorKey][periods[nextPeriod]]) < totalPeriod

					if isUseMoreThanOnePeriod && nextPeriodHasTask && nextPeriodMoreThanTotalPeriod {
						delete(doctorPeriods[doctorKey], periodKey)
					}

					totalDuration -= maxDuration
					index += 1
				}
			}
		}
	}

	fmt.Printf("\n %+v \n", doctorPeriods)

	// หมอคนไหนไม่มีช่วงเวลาให้ pop ออก

	resDoctors = utils.SortTimes(resDoctors)
	index := 0
	for doctorKey := range doctorPeriods {
		if len(doctorPeriods[doctorKey]) < 1 {
			resDoctors = utils.RemoveItemFromArray(resDoctors, index)
			index -= 1
		}
		index += 1
	}

	fmt.Printf("\n %+v \n", resDoctors)
}

func getIndexByValue[T comparable](slice []T, value T) int {
	for i, v := range slice {
		if v == value {
			return i
		}
	}
	return -1
}
